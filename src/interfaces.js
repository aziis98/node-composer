
const R = require('ramda');
const ContextManager = require('./context-manager');

/**
 * Static class for interacting with the project object
 */
class IProject {

	/**
	 * Ok magari forse non a questo livello però credo che questa 
	 * classe potrebbe essere utile
	 */
	static getContextName(project) {
		return project.context;
	}

	static getNodeIOLayout(project, node) {

		const t = ContextManager.getContextByName(project.context).primitives[node.type];	
		return { inputs: t.inputs, outputs: t.outputs };

	}

	/**
	 * TODO: Eventually change to "getNodeKind" with possible values being "root", "group", "node"
	 */
	static getNodeType(project, node) {
		const t = ContextManager.getContextByName(project.context).primitives[node.type];

		if (t)
			return t.type;
		else
			return node.type;
	}

	static getNodeLayout(project, node) {
		return node.layout;
	}

	static getNodes(node) {
		return R.values(node.nodes);
	}

}

module.exports = { IProject };