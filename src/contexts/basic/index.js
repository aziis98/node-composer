
const { NodeTypes } = require('../../constants');

const BasicNode = {
	name: 'Node',
	type: NodeTypes.Node,
	inputs: [
		{ label: 'In' },
	],
	outputs: [
		{ label: 'Out' },
	]
}

const BasicGroup = {
	name: 'Group',
	type: NodeTypes.Group,
	inputs: [
		{ label: 'In' },
	],
	outputs: [
		{ label: 'Out' },
	]
}

const BasicContext = {
	name: 'BasicContext',
	primitives: { BasicNode, BasicGroup }
};

module.exports = BasicContext;