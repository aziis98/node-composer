
const crypto = require('crypto');

module.exports = {
	randomHash() {
		return crypto.randomBytes(20).toString('hex');
	},

}