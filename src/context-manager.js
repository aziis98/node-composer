
const util = require('util');
const R = require('ramda');

const createContextMap = R.pipe(
	R.map(context => [context.name, context]),
	R.fromPairs
);

const contexts = createContextMap([
	require('./contexts/basic/index')
]);

class ContextManager {
	
	static hasContext(name) {
		return !!contexts[name];
	}

	static getContextByName(name) {
		return contexts[name];
	}

}

module.exports = ContextManager;