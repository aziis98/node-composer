
const IProject = require('./interfaces');

const ContextManager = require('./context-manager');
const UI = require('./ui/ui');
const { EVENTS, EMPTY_PROJECT } = require('./constants');

const R = require('ramda');

const util = require('util');
const fs = require('fs');
const readFile = util.promisify(fs.readFile);

const { ipcRenderer, remote } = require('electron');

const ui = new UI();

async function loadProject(path) {
	
	const project = JSON.parse(await readFile(path))

	if (ContextManager.hasContext(project.context)) {
		ui.setProject(project);
	}
	else {
		remote.dialog.showMessageBox({
			type: 'error',
			message: `The context named "${ project.context }" is not implemented!`,
		})
	}

}

ipcRenderer.on(EVENTS.PROJECT_OPEN, (e, path) => {
	loadProject(path);
})

// Load example project
loadProject('./examples/project-1.json')
	.catch(e => console.error('Unable to open example project!', e));