
const IProject = require('../interfaces');
const { el, setAttr, setStyle } = require('redom');

/**
 * @param {CanvasRenderingContext2D} g 
 */
function renderConnection(g, { from: [x1, y1], to: [x2, y2] }) {
	g.strokeStyle = 'rgb(75, 142, 243)';
	g.lineWidth = 1.5;
	g.lineJoin = 'round';

	const OFFSET = 15;

	g.beginPath();
	g.moveTo(x1, y1);
	g.lineTo(x1 + OFFSET, y1);
	g.lineTo(x2 - OFFSET, y2);
	g.lineTo(x2, y2);
	g.stroke();
}

/**
 * Computes the position of a connector given: node, input/output, port 
 * @sideEffetcs documente.getElementById
 */
function getConnectorPosition({ offset: [ox, oy], node, type, port }) {

	const mean = (...args) => R.sum(args) / args.length;

	const { top, bottom, left, right } = document
		.getElementById('c-' + node.id + '-' + type[0] + '-' + port)
		.getBoundingClientRect();

	return [mean(left, right) - ox, mean(top, bottom) - oy];

}


class ConnectionLayer {

	constructor() {

		this.el = el('canvas');
		this.ctx = this.el.getContext('2d');
		
	}

	renderConnections() {

		this.ctx.resetTransform();
		this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
		this.ctx.translate(0.5, 0.5);

		const r = this.el.getBoundingClientRect();
		const offset = [r.left, r.top];

		this.node.connections.forEach(connection => {
			if (connection.type === 'in-out') {

				const pos1 = getConnectorPosition({
					offset,

					node: this.node.nodes[connection.from],
					type: 'output',
					port: connection.fromPort
				});
				const pos2 = getConnectorPosition({
					offset,

					node: this.node.nodes[connection.to],
					type: 'input',
					port: connection.toPort
				});

				renderConnection(this.ctx, { from: pos1, to: pos2 });
			}
			else if (connection.type === 'in') {

				const pos1 = getConnectorPosition({
					offset,

					node: this.node,
					type: 'input',
					port: connection.fromPort
				});

				const pos2 = getConnectorPosition({
					offset,

					node: this.node.nodes[connection.to],
					type: 'input',
					port: connection.toPort
				});

				renderConnection(this.ctx, { from: pos1, to: pos2 });
			}
			else if (connection.type === 'out') {

				const pos1 = getConnectorPosition({
					offset,

					node: this.node.nodes[connection.from],
					type: 'output',
					port: connection.fromPort
				});

				const pos2 = getConnectorPosition({
					offset,

					node: this.node,
					type: 'input',
					port: connection.toPort
				});

				renderConnection(this.ctx, { from: pos1, to: pos2 });
			}
			else {
				throw 'Invalid connection type: ' + connection;
			}
		});
	}

	update({ project, node, dimensions: [width, height] }) {

		this.project = project;
		this.node = node;

		if (this.el.width !== width || this.el.height !== height) {
			setAttr(this.el, { width, height });
		}

		setTimeout(() => {
			this.renderConnections();
		}, 0);

	}

}

module.exports = { ConnectionLayer };