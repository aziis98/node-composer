
class DragHandler {
	
	constructor($root) {

		this.$root = $root;

		this.dragStartPos = null;
		this.dragging = false;

		this.$root.addEventListener('mousemove', this.onMouseMoved.bind(this));
		this.$root.addEventListener('mouseup', this.onMouseUp.bind(this));
		
	}

	requestDragStart(origin, onDrag, onDragEnd) {
		if (!this.dragging) {
			
			this.origin = origin;
			this.onDrag = onDrag;
			this.onDragEnd = onDragEnd;
			this.dragging = true;

			return true;
		}
		else {
			return false;
		}
	}

	onMouseMoved(e) {
		if (this.dragging) {

			if (!this.dragStartPos) {
				// Setup drag
				this.dragStartPos = [e.x, e.y];
			}

			const [sx, sy] = this.dragStartPos;

			const dx = e.x - sx;
			const dy = e.y - sy;

			this.onDrag({ origin: this.origin, delta: [dx, dy] });

		}
	}

	onMouseUp(e) {
		this.dragging = false;
		this.dragStartPos = null;
		this.origin = null;
		this.onDrag = null;

		if (this.onDragEnd) {
			this.onDragEnd(e);
			this.onDragEnd = null;
		}
	}

}

module.exports = { DragHandler };