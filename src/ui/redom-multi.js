
const R = require('ramda');
const { el, place, mount } = require('redom');

/**
 * @see MultiPlace
 */
function multi(parent, desc, initCase, initValue) {
	return new MultiPlace(parent, desc, initCase, initValue);
}

class MultiPlace {
	
	/**
	 * 
	 * The descriptor is an object containing an `elements` and a `cases` property.
	 * 
	 * The `elements` property is an object with all the possible elements
	 * that can appear in this element. (Uses the fact that propeties are 
	 * ordered in JS, so this order will be the one in the DOM.)
	 * 
	 * The `cases` property is an object defining templates for the various cases.
	 * 
	 * Keep in mind that the elements property cannot change after the constructor call, while one 
	 * could add more cases after the creation.
	 * 
	 * @param {*} parent REDOM parent element or name
	 * @param {{ elements: Object.<string, HTMLElement>, cases: Object.<string, string | string[]> }} desc Descriptor object.
	 * @param {*} initCase 
	 * @param {*} initMap 
	 */
	constructor(parent, desc, initCase, initMap) {
		
		const { elements, cases } = desc;

		this.views = elements;

		this.elements = R.mapObjIndexed((el, label) => place(el, initMap[label]), elements);

		this.cases = R.mapObjIndexed(
			(template, templateName) => new Set(
				R.isArrayLike(template) ? template : template.split(' ')
			), 
			cases
		);
		
		this.el = el(parent, R.values(this.elements));
		
		R.forEachObjIndexed(el => el.update(false), this.elements);

	}

	/**
	 * Updates all the palceholders based on wether they 
	 * are present in the current case template or not.
	 * 
	 * @param {string} newCase Update to this new case
	 * @param {Object.<string, any>} updateObj Each updated element gets passed `updateObj[label]`.
	 */
	update(newCase, updateObj) {
		R.forEachObjIndexed(
			(el, label) => el.update(
				this.cases[newCase].has(label),
				updateObj ? updateObj[label] : undefined
			),
			this.elements
		);
	}
	
}

module.exports = { multi, MultiPlace };