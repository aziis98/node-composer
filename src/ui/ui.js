
const { el, list, place, router, setAttr, setStyle, mount } = require('redom');
const R = require('ramda');

const { IProject } = require('../interfaces');
const { NodeGroup } = require('./ui-editor');
const { DragHandler } = require('./drag');

class StatusBar {
	constructor() {
		this.el = el('#status-bar', [
			el('span.label', [
				el('span.name', 'Context:'),
				this.contextName = el('span.value')
			])
		]);
	}

	update({ contextName }) {
		this.contextName.innerText = contextName;
	}
}

/**
 * Manages all the UI interactions and sets up the various 
 * DOM nodes and event handlers.
 */
class UI {

	setProject(project) {

		this.project = project;

		this.displayMain();
		this.displayStatusBar();

	}

	registerEvents() {

		window.addEventListener('resize', e => {
			this.root.updateConnectionLayer();
		});

	}

	displayMain() {
		const main = el('#main');
		
		this.dragHandler = new DragHandler(main);
		
		this.root = new NodeGroup({
			dragHandler: this.dragHandler,
			project: this.project,
			parent: null
		});
		
		this.root.update({
			type: 'root',
			nodes: this.project.nodes,
			connections: this.project.connections,
			layout: this.project.layout,
		});

		mount(main, this.root);
		mount(document.body, main);
		
		this.registerEvents();
	}

	displayStatusBar() {
		const statusBar = new StatusBar();

		statusBar.update({ contextName: this.project.context });

		mount(document.body, statusBar);
	}

};

module.exports = UI;