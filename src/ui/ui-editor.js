
const { el, list, place, router, setAttr, setStyle, mount } = require('redom');
const { multi } = require('./redom-multi');
const R = require('ramda');

const { ConnectionLayer } = require('./connections');
const { IProject } = require('../interfaces');

Element.prototype.clearChildren = function() {
	while (this.firstChild)
		this.removeChild(this.firstChild);
}

class Connector {

	constructor() {
		this.el = el('.connector');
	}

	update({ type, id, port }) {
		setAttr(this.el, {
			id: 'c-' + id + '-' + type[0] + '-' + port,
			'class': 'connector ' + type
		});
	}

}

class ConnectorsColumn {

	constructor(type) {
		this.type = type;
		this.el = list('div.' + type, Connector);
	}

	update({ node, connectors }) {
		this.el.update(connectors.map(
			(c, i) => ({ ...c, type: this.type, id: node.id, port: i })
		));
	}
}

class UITitle {

	constructor() {
		this.el = el('.title', this.t = el('span'));
	}

	update(text) {
		this.t.textContent = text;
	}

}

/*
class NodeGroupOld {

	constructor() {

		// Element Structure
		this.el = el('div',
			this.bgLayer = place(ConnectionLayer),
			this.nodes = list('.nodes', NodeGroup),
			this.inputs = place(ConnectorsColumn, 'input'),
			this.outputs = place(ConnectorsColumn, 'output'),
			this.title = place(UITitle),
		);

		// this.el.addEventListener('mousedown', this.onMouseDown.bind(this));

		// this.el.addEventListener('click', e => {

		// 	if (this.nodeType === 'group' && e.target.parentElement === this.title.el) {

		// 		const isHidden = !!this.nodes.el.style.display;

		// 		if (isHidden) {
		// 			this.updateAsGroup();
		// 		}
		// 		else {
		// 			this.updateAsNode();
		// 		}

		// 	}
		// });
	}

	update(node, i, nodes, { dragHandler, project, parent }) {

		this.dragHandler = dragHandler;
		this.project = project;
		this.parent = parent;
		this.node = node;
		this.nodeType = IProject.getNodeType(this.project, this.node);

		// console.log(this.nodeType, this.node.id);

		if (this.nodeType === 'node') {
			this.updateAsNode();
		}
		else if (this.nodeType === 'group') {
			this.updateAsGroup();
		}
		else if (this.nodeType === 'root') {
			this.updateAsRoot();
		}

	}

	// Event Handler //

	onMouseDown(e) {
		if (this.nodeType !== 'root' && e.button === 0) {
			if (e.target.classList.contains('connector')) {
				// TODO: Handle connection creation
			}
			else {
				this.dragHandler.requestDragStart(this.node.layout.pos, this.onDrag.bind(this));
			}
		}
	}

	onDrag({ origin: [ox, oy], delta: [dx, dy] }) {

		this.node.layout.pos = [ox + dx, oy + dy];

		const [x, y] = this.node.layout.pos;

		setStyle(this.el, {
			transform: `translate(calc(${x}px - 50%), calc(${y}px - 50%))`
		});

		this.parent.updateConnections();

	}

	// Update Methods //

	updateAsNode() {
		this.updateContainer(false);
		this.updateIO();

		this.title.update(true, this.node.title);

		this.updateElementAttrs();
		this.updateElementPosition();
	}

	updateAsGroup() {
		this.updateContainer(true);
		this.updateIO();

		this.title.update(true, this.node.title);

		this.updateElementAttrs();
		this.updateElementDimensions();
		this.updateElementPosition();
	}

	updateAsRoot() {
		this.updateContainer(true);

		this.inputs.update(false);
		this.outputs.update(false);

		this.title.update(false);

		this.updateElementAttrs();
	}

	// Update part of component

	// updateElementAttrs() {
	// 	setAttr(this.el, { 'class': this.nodeType });

	// 	if (this.nodeType !== 'root') {
	// 		setAttr(this.el, {
	// 			id: this.nodeType + '-' + this.node.id,
	// 			'class': this.nodeType
	// 		});
	// 	}
	// }

	// updateBackgroundLayer(visibile) {

	// 	const windowDims = [innerWidth, innerHeight];
	// 	const dimensions = this.nodeType === 'root' ? windowDims : this.node.layout.dimensions;

	// 	this.bgLayer.update(visibile, { project: this.project, node: this.node, dimensions });
	// }

	/**
	 * Used by group and root with visibile=true and node with visible=falses
	 *
	// updateContainer(visible) {
	// this.updateBackgroundLayer(visible);

	// setStyle(this.nodes, { display: visible ? null : 'none' });

	// const ns = R.values(this.node.nodes) || [];
	// console.log(ns);

	// this.nodes.update(ns, { 
	// 	dragHandler: this.dragHandler, 
	// 	project: this.project,
	// 	parent: this
	// });
	// }

	// updateIO() {
	// 	const attachFn = list => ({ node: this.node, connectors: list });
	// 	const { inputs, outputs } = IProject.getNodeIOLayout(this.project, this.node);

	// 	this.inputs.update(true, attachFn(inputs));
	// 	this.outputs.update(true, attachFn(outputs));
	// }

	// updateElementPosition() {
	// 	const [x, y] = this.node.layout.pos;

	// 	setStyle(this.el, {
	// 		transform: `translate(calc(${x}px - 50%), calc(${y}px - 50%))`
	// 	});
	// }

	// updateElementDimensions() {
	// 	const [width, height] = this.getDimensions();

	// 	setStyle(this.el, {
	// 		width: `${width}px`,
	// 		height: `${height}px`
	// 	});
	// }

	// getDimensions() {
	// 	if (this.nodeType === 'node')
	// 		return null;
	// 	else
	// 		return (this.node.layout && this.node.layout.dimensions) || [innerWidth, innerHeight];
	// }

}
*/

class NodeGroup {

	constructor({ dragHandler, project, parent }) {
		this.dragHandler = dragHandler;
		this.project = project;
		this.parent = parent;

		this.dragging = false;

		this.el = el('div');
	}

	setupEvents() {
		this.el.onmousedown = e => this.onMouseDown(e);
		this.el.onmouseup = e => this.onMouseUp(e);
	}

	onMouseDown(e) {

		if (e.button === 0) {
			if (this.node._nodeType !== 'root') {
				if (e.target.classList.contains('connector')) {
					// TODO: Handle connection creation
				}
				else {
					this.dragHandler.requestDragStart(this.node.layout.pos, e => this.onDrag(e));
				}
			}
			else {
				// const origins = R.values(this.node.nodes).map(it => it.layout.pos);
				this.dragHandler.requestDragStart(this.node.layout.pan, e => this.onPan(e));
			}
		}
	}

	onPan({ origin: [ox, oy], delta: [dx, dy] }) {		
		this.node.layout.pan = [ ox + dx, oy + dy ];
		this.displayAsRoot();
	}

	onDrag({ origin: [ox, oy], delta: [dx, dy] }) {
		this.node.layout.pos = [ox + dx, oy + dy];
		this.updateElementPosition();
		this.parent.updateConnectionLayer();

		this.dragging = true;
	}

	onMouseUp(e) {
		
		if (this.node._nodeType === 'group' 
				&& !this.dragging 
				&& e.button === 0 
				&& e.target.classList.contains('title')) {

			const collapsed = this.node.layout.collapsed;

			if (collapsed) {
				this.displayAsGroup(true);
			}
			else {
				this.displayAsNode(true);
			}

			this.node.layout.collapsed = !collapsed;
		}
	}

	update(node) {
		const map = {
			node: this.displayAsNode,
			group: this.displayAsGroup,
			root: this.displayAsRoot
		};

		this.node = node;
		this.node._nodeType = IProject.getNodeType(this.project, this.node);

		setAttr(this.el, {
			'class': this.node._nodeType
		});

		if (this.node._nodeType !== 'root') {
			setAttr(this.el, {
				'id': this.node._nodeType + '-' + this.node.id
			});
		}

		map[this.node._nodeType].call(this, this.node);

		this.setupEvents();

	}

	displayAsNode(force = false) {
		if (!this.el.children.length || force) {
			this.el.clearChildren();

			mount(this.el, el('.title', this.node.title));
			this.displayIO();
		}
		
		this.updateIO();
		this.updateElementPosition();
	}

	displayAsGroup(force = false) {
		if (!this.el.children.length || force) {
			this.el.clearChildren();

			mount(this.el, el('.title', this.node.title));
			mount(this.el, this.bgLayer = new ConnectionLayer());
			mount(this.el, this.nodes = list('.nodes', NodeGroup, null, { ...this, parent: this }));
			this.displayIO(this.node);
		}
		
		this.updateIO();
		this.updateContainer();
		this.updateElementPosition();
		this.updateElementDimensions();
	}

	displayAsRoot() {
		if (!this.el.children.length) {
			mount(this.el, this.bgLayer = new ConnectionLayer());
			mount(this.el, this.nodes = list('.nodes', NodeGroup, null, { ...this, parent: this }));
		}

		this.updateContainer();
	}

	updateContainer() {
		this.nodes.update(R.values(this.node.nodes));
		this.updateConnectionLayer();
	}

	// TODO: Make this less magical
	updateConnectionLayer() {
		const isRoot = this.node._nodeType === 'root';
		let [w, h] = isRoot ? [innerWidth, innerHeight] : this.node.layout.dimensions;

		h -= isRoot ? 30 : 25;

		this.bgLayer.update({ project: this.project, node: this.node, dimensions: [w, h] });
	}

	displayIO(force) {
		if (!this.inputs && !this.outputs) {
			mount(this.el, this.inputs = new ConnectorsColumn('input'));
			mount(this.el, this.outputs = new ConnectorsColumn('output'));
		}
	}
	
	updateIO() {
		const attachFn = list => ({ node: this.node, connectors: list });
		const { inputs, outputs } = IProject.getNodeIOLayout(this.project, this.node);

		this.inputs.update(attachFn(inputs));
		this.outputs.update(attachFn(outputs));
	}

	updateElementPosition() {
		
		let [x, y] = this.node.layout.pos;

		if (this.parent.node._nodeType === 'root') {
			const [px, py] = this.parent.node.layout.pan;
			[x, y] = [x + px, y + py];	
		}

		setStyle(this.el, {
			transform: `translate(calc(${x}px - 50%), calc(${y}px - 50%))`
		});
	}

	updateElementDimensions() {

		if (this.node.layout.collapsed) {
			this.el.classList.add('collapsed');
			
			setStyle(this.el, {
				width: `unset`,
				height: `unset`
			});
		}
		else {
			const [width, height] = this.node.layout.dimensions;
	
			this.el.classList.remove('collapsed');
	
			setStyle(this.el, {
				width: `${width}px`,
				height: `${height}px`
			});
		}

	}

}

class NodeGroupNew {

	constructor({ ui, parent, node }) {
		this.ui = ui;
		this.parent = parent;
		this.node = node;

		this.node._kind = IProject.getNodeType(this.ui.project, this.node);

		this.el = multi('.' + this.node._kind, {
			elements: {

			},
			cases: {
				root: 'bgLayer nodes',
				node: 'title io',
				group: 'title bgLayer nodes io'
			}
		});

		// this.el = el('.' + this.node._kind, [
		// 	this.elTitle = place(...)
		// 	this.elNodes = place(list('.nodes', NodeGroup, null, { ui, parent, node })),
		// ]);
	}

	update() {

	}

}

module.exports = { NodeGroup };