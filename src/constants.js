
const { randomHash } = require('./utils.js');

const hash1 = randomHash();
const hash2 = randomHash();

module.exports = {

	NodeTypes: {
		Node: 'node',
		Group: 'group'
	},

	EVENTS: {
		PROJECT_OPEN: 'project:open'
	},
	EMPTY_PROJECT: {
		title: '<Untitled>',
		context: 'BasicContext',

		nodes: {
			hash1: {
				id: hash1,
				title: 'A',
				type: 'BasicNode'
			},
			hash2: {
				id: hash2,
				title: 'B',
				type: 'BasicNode'
			}
		},
		connections: [
			{ type: 'in-out', from: hash1, to: hash2 }
		],

		layout: {
			nodes: {
				hash1: {
					pos: [200, 200]
				},
				hash2: {
					pos: [400, 300]
				}
			}
		}
	}
}