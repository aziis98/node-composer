
![Node Example 1](./examples/image-1.png)

# Node Composer

Create and manipulate node graphs. The idea behind this application is to create visually a node graph structure and let other application generate things from it.

# Primitives

There is only one primitive, the "group". A group can be collapsed or open, this can be toggled by `Ctrl+Click` on the group name.

[All drawings are temporary]

```
	[ Node 1 ]
```
_A collapsed group_


```
	+-----< Node 1 >-----+
	|                    |
	|                    |
	|                    |
	|                    |
	|                    |
	+--------------------+
```
_The same group opened_

# Contexts

The application is based on contexts. Each one will provide different functionalities to edit a specific kind of graph. In other words the context provides the semantic to a node-graph.

## Free Context

In this context you are free to sketch any graph without and semantic restriction.

# Widgets & Styling

Nodes, connections can all be styled and there are various "widgets" (slider for values etc.) that can be present on a node.

For example to change the appearance of a node in the FreeContext you can just open the node and add a node with the name `{<propety>=<value>}` (Could be made work in any _context_, could be good for customization of random nodes)

```
	+------------< Node 1 >-------------+
	|                                   |
	|   [ {style.background: #f00} ]    |
	|                                   |
	+-----------------------------------+
```

## Properties

In the future there will be a propeties panel to easily chage all of these. But internally this is the way they will be stored inside the node. A property has the following structure

```
<property> ("." <property> )* ":" <value>
```

### General

- `type: <type>` Typechecks the name of this node to match the provided type. This will highlight in red the node if the type doesn't match. The currenly valid types are:
	
	- `string` Default matcher, matches anything.

	- `natural` Only matches natural numbers 
	
		0, 1, 2, ...

	- `integer` Only matches integer numbers
		
		0, 1, -1, 2, -2, ...

	- `decimal` Matches integers and decimal numbers 
	
		-12, +3.14, ...

	- `color` Matches most CSS valid color formats, maybe. (
		
		red, #f00, #ff0000, ...

- `editable: <boolean>` By default this is true. If this is false clicking on the name wont transform it to a textfield for editing.

### Style

- `style.color: <color>` Sets the background color of a node
- `style.output.<port>.color: <color>` Sets the color the connector on the port-th output of the node.
- `style.output.<port>.label: <text>` Sets the label the connector on the port-th output of the node. This appears near the output connector or maybe near the mouse when the connection is hovered from far away.

[TODO: Style also input connections? but then it may cause conflicts.]

### Widgets

- `widget: slider` If the type of this node is _numerical_ then changes the interface of this node to a slider. The following parameters are allowed:

	- `widget.min: <value>` Minimum value of the slider.
	- `widget.max: <value>` Maximum value of the slider.
	- `widget.step: <value>` Step of the slider.

- `widget: color` If the type of this node is _color_ then chages the interface to show a color picker.

# To Use

```bash
# Clone this repository
git clone https://gitlab.com/aziis98/node-composer.git
# Go into the repository
cd node-composer
# Install dependencies
npm install
# Run the app
npm start
```

# License

[MIT](LICENSE.md)
